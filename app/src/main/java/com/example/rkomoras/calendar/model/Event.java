package com.example.rkomoras.calendar.model;

import org.joda.time.DateTime;

public class Event {
	private DateTime startTime;
	private DateTime endTime;
	private boolean wholeDay;
	private String title;
	private String description;
	private int color;

	public Event(DateTime startTime, DateTime endTime, String title, String description, boolean wholeDay, int color) {
		this.description = description;
		this.endTime = endTime;
		this.startTime = startTime;
		this.title = title;
		this.wholeDay = wholeDay;
		this.color = color;
	}

	public DateTime getEndTime() {
		return endTime;
	}

	public DateTime getStartTime() {
		return startTime;
	}

	public boolean isWholeDay() {
		return wholeDay;
	}

	public String getDescription() {
		return description;
	}

	public String getTitle() {
		return title;
	}

	public int getColor() {
		return color;
	}
}
