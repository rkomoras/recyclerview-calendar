package com.example.rkomoras.calendar;

import android.app.Application;

import net.danlew.android.joda.JodaTimeAndroid;

public class CalendarApp extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		JodaTimeAndroid.init(this);
	}
}
