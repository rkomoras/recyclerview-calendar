package com.example.rkomoras.calendar.calendar;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.LinearLayout;

import com.example.rkomoras.calendar.R;
import com.example.rkomoras.calendar.model.Event;
import com.example.rkomoras.calendar.util.Pair;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.YearMonth;

import java.util.List;
import java.util.Map;

public class CalendarRecyclerView extends RecyclerView implements CalendarAdapter.ListenerView {
	private static final int VELOCITY_THRESHOLD = 700;
	public boolean scrolling = false;
	private VelocityTracker velocityTracker;
	private CalendarAdapter adapter;

	public CalendarRecyclerView(Context context) {
		this(context, null);
	}

	public CalendarRecyclerView(Context context, @Nullable AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public CalendarRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		velocityTracker = VelocityTracker.obtain();
		setLayoutManager(new GridLayoutManager(getContext(), DateTimeConstants.DAYS_PER_WEEK, LinearLayoutManager.VERTICAL, false) {
			@Override
			public boolean canScrollVertically() {
				return true;
			}

			@Override
			public boolean canScrollHorizontally() {
				return false;
			}

			@Override
			public boolean supportsPredictiveItemAnimations() {
				return true;
			}
		});
		setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		getItemAnimator().setChangeDuration(0);
		addItemDecoration(new RecyclerView.ItemDecoration() {
			@Override
			public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
				int space = getResources().getDimensionPixelSize(R.dimen.grid_spacing);
				outRect.bottom = space;
				if (parent.getChildLayoutPosition(view) % DateTimeConstants.DAYS_PER_WEEK != 0) {
					outRect.left = space;
				}
			}
		});
	}

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		return scrolling;
	}

	@Override
	public void setAdapter(Adapter adapter) {
		super.setAdapter(adapter);
		this.adapter = (CalendarAdapter) adapter;
		this.adapter.setListenerView(this);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent e) {
		final int action = MotionEventCompat.getActionMasked(e);

		if (action == MotionEvent.ACTION_CANCEL) {
			return false;
		}

		if (scrolling) {
			return true;
		}

		velocityTracker.addMovement(e);

		if (action == MotionEvent.ACTION_UP) {
			velocityTracker.computeCurrentVelocity(1000);
			float velY = VelocityTrackerCompat.getYVelocity(velocityTracker, 0);
			float velX = VelocityTrackerCompat.getXVelocity(velocityTracker, 0);
			velocityTracker.clear();

			if (Math.abs(velY) < Math.abs(velX)) {
				return false;
			} else if (velY > VELOCITY_THRESHOLD) {
				move(false);
				return true;
			} else if (velY < -VELOCITY_THRESHOLD) {
				move(true);
				return true;
			}

			return false;
		}
		return false;
	}

	public void move(final boolean forward) {
		if (!scrolling) {
			adapter.setFirstNewPosition(((GridLayoutManager) getLayoutManager()).findFirstVisibleItemPosition());
			final int scrollSize = adapter.move(forward);
			scrolling = true;
			postDelayed(new Runnable() {
				@Override
				public void run() {
					int spacing = getResources().getDimensionPixelSize(R.dimen.grid_spacing);
					smoothScrollBy(0, (forward ? 1 : -1) * ((getMeasuredHeight() - (Calendar.MAX_MONTH_HEIGHT - 1) * spacing) / Calendar.MAX_MONTH_HEIGHT + spacing) * scrollSize);
				}
			}, 50);
		}
	}

	@Override
	public void onScrollStateChanged(int state) {
		if (state != SCROLL_STATE_IDLE) {
			scrolling = true;
		} else {
			scrolling = false;
			super.onScrollStateChanged(state);
		}
	}

	public DateTime getSelectedDate() {
		return ((CalendarAdapter) getAdapter()).getSelectedDate();
	}

	public YearMonth getSelectedMonth() {
		return ((CalendarAdapter) getAdapter()).getSelectedMonth();
	}

	public void addEvent(Event event) {
		adapter.addEvent(event);
	}

	public void setEvents(Map<DateTime, List<Event>> events) {
		adapter.setEvents(events);
	}

	public Pair<DateTime, DateTime> getDateRange() {
		return adapter.getDateRange();
	}

	@Override
	public void scrollToItem(int position) {
//		Log.v("SCROLL TO ITEM", Integer.toString(position));
		int mHeight = getMeasuredHeight();
		if (mHeight == 0) {
			getLayoutManager().scrollToPosition(position);
		} else {
			int spacing = getResources().getDimensionPixelSize(R.dimen.grid_spacing);
			int singleItemHeightWithMargin = (mHeight - (Calendar.MAX_MONTH_HEIGHT - 1) * spacing) / Calendar.MAX_MONTH_HEIGHT + spacing;
			int offset = (int) Math.floor(position / DateTimeConstants.DAYS_PER_WEEK) * singleItemHeightWithMargin;

//			scrollTo(0, offset); // NELZE!!!
			int fvip = ((GridLayoutManager) getLayoutManager()).findFirstCompletelyVisibleItemPosition();
			int fvio = singleItemHeightWithMargin * (int) Math.floor(fvip / DateTimeConstants.DAYS_PER_WEEK);
			smoothScrollBy(0, offset - fvio); // omg
			// TODO zrusit smooth a misto toho ukazat asi nejakej loading!
		}
	}

	public void goToToday() {
		int pos = adapter.goToToday();
		if (pos >= 0) {
			scrollToItem(pos);
		}
	}
}
