package com.example.rkomoras.calendar;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.rkomoras.calendar.calendar.Calendar;
import com.example.rkomoras.calendar.calendar.CalendarAdapter;
import com.example.rkomoras.calendar.model.Event;
import com.example.rkomoras.calendar.model.MockEventRepository;
import com.example.rkomoras.calendar.util.Pair;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;

import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements CalendarAdapter.CalendarListener {
	private static final String SELECTED_DATE_KEY = "selected_date";
	private static final String SELECTED_MONTH_KEY = "selected_month";
	private static final String SELECTED_RANGE_START_KEY = "selected_range_start";
	private static final String SELECTED_RANGE_END_KEY = "selected_range_end";
	private Calendar calendar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		calendar = (Calendar) findViewById(R.id.calendar);

		DateTime selectedDate = null;
		YearMonth selectedMonth = null;
		Pair<DateTime, DateTime> range = null;
		if (savedInstanceState != null && savedInstanceState.getSerializable(SELECTED_DATE_KEY) != null) {
			selectedDate = (DateTime) savedInstanceState.getSerializable(SELECTED_DATE_KEY);
			selectedMonth = (YearMonth) savedInstanceState.getSerializable(SELECTED_MONTH_KEY);
			range = new Pair<>((DateTime) savedInstanceState.getSerializable(SELECTED_RANGE_START_KEY), (DateTime) savedInstanceState.getSerializable(SELECTED_RANGE_END_KEY));
		} else {
			range = calendar.getDateRange();
		}

		Log.v("MainActivity", "Date Range: " + CalendarAdapter.debugFormatter.print(range.first) + " - " + CalendarAdapter.debugFormatter.print(range.second));
		calendar.init(this, selectedDate, selectedMonth, MockEventRepository.getInstance().getEventsForRange(range.first, range.second));
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		DateTime selectedDate = calendar.getSelectedDate();
		YearMonth selectedMonth = calendar.getSelectedMonth();
		Pair<DateTime, DateTime> range = calendar.getDateRange();
		outState.putSerializable(SELECTED_DATE_KEY, selectedDate);
		outState.putSerializable(SELECTED_MONTH_KEY, selectedMonth);
		outState.putSerializable(SELECTED_RANGE_START_KEY, range.first);
		outState.putSerializable(SELECTED_RANGE_END_KEY, range.second);
		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.goToToday:
				calendar.goToToday();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void notifyAdd(DateTime start, DateTime end) {
		Log.v("ADD", "adding new items " + CalendarAdapter.debugFormatter.print(start) + " - " + CalendarAdapter.debugFormatter.print(end));
		Map<DateTime, List<Event>> newEvents = MockEventRepository.getInstance().getEventsForRange(start, end);
		calendar.setEvents(newEvents);
	}

	@Override
	public void notifyMonthChanged(YearMonth month) {
		getSupportActionBar().setTitle(Calendar.getMonthString(month));
	}
}
