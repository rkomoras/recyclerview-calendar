package com.example.rkomoras.calendar.model;

import android.graphics.Color;

public class EventColors {
	public static final int RED = Color.parseColor("#F44336");
	public static final int PURPLE = Color.parseColor("#9C27B0");
	public static final int INDIGO = Color.parseColor("#3F51B5");
	public static final int BLUE = Color.parseColor("#2196F3");
	public static final int TEAL = Color.parseColor("#009688");
	public static final int GREEN = Color.parseColor("#4CAF50");
	public static final int DEEP_ORANGE = Color.parseColor("#FF5722");
	public static final int BROWN = Color.parseColor("#795548");
	public static final int BLUE_GREY = Color.parseColor("#607D8B");
}
