package com.example.rkomoras.calendar.model;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

public interface EventRepository {
	Map<DateTime, List<Event>> getEventsForRange(DateTime start, DateTime end);
	List<Event> getEventsForDay(DateTime day);
}
