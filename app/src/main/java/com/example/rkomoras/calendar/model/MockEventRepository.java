package com.example.rkomoras.calendar.model;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class MockEventRepository implements EventRepository {
	private NavigableMap<DateTime, List<Event>> events;
	private static MockEventRepository mockEventRepository;

	public static MockEventRepository getInstance() {
		if (mockEventRepository == null) {
			mockEventRepository = new MockEventRepository();
		}
		return mockEventRepository;
	}

	private MockEventRepository() {
		// fill data
		events = new TreeMap<>(DateTimeComparator.getDateOnlyInstance());

		DateTime today = new DateTime().withTimeAtStartOfDay();
		List<Event> todayEvents = new ArrayList<>();
		todayEvents.add(new Event(today.withTime(9, 0, 0, 0), today.withTime(17, 0, 0, 0), "Práce", "Android vs. iOS", false, EventColors.TEAL));
		events.put(today, todayEvents);

		DateTime tomorrow = today.plusDays(1);
		List<Event> tomorrowEvents = new ArrayList<>();
		tomorrowEvents.add(new Event(tomorrow.withTime(9, 0, 0, 0), tomorrow.withTime(17, 0, 0, 0), "Zase práce", "Android vs. iOS", false, EventColors.TEAL));
		tomorrowEvents.add(new Event(tomorrow.withTime(8, 0, 0, 0), tomorrow.withTime(19, 0, 0, 0), "WTF", "No (intellij) idea", true, EventColors.INDIGO));
		events.put(tomorrow, tomorrowEvents);

		DateTime mmes = new DateTime(2016, 6, 15, 0, 0);
		List<Event> mmesEvents = new ArrayList<>();
		mmesEvents.add(new Event(mmes.withTime(15, 0, 0, 0), mmes.withTime(17, 0, 0, 0), "Mango", "ve vlastni stave", false, EventColors.RED));
		mmesEvents.add(new Event(mmes.withTime(18, 0, 0, 0), mmes.withTime(21, 0, 0, 0), "Kiwi", "v oleji", false, EventColors.DEEP_ORANGE));
		events.put(mmes, mmesEvents);

		DateTime wwut = new DateTime(2016, 8, 2, 0, 0);
		List<Event> wwutEvents = new ArrayList<>();
		wwutEvents.add(new Event(wwut.withTime(15, 0, 0, 0), wwut.withTime(17, 0, 0, 0), "Broskev", "se slupkou", false, EventColors.BLUE));
		events.put(wwut, wwutEvents);

		DateTime bday = new DateTime(2016, 3, 30 , 0, 0);
		List<Event> birthdayEvents = new ArrayList<>();
		birthdayEvents.add(new Event(bday.withTimeAtStartOfDay(), null, "BRZDAY", "skjskalna", true, EventColors.GREEN));
		events.put(bday, birthdayEvents);
	}

	@Override
	public List<Event> getEventsForDay(DateTime day) {
		return events.get(day);
	}

	@Override
	public Map<DateTime, List<Event>> getEventsForRange(DateTime start, DateTime end) {
		Map<DateTime, List<Event>> result = new HashMap<>();

		NavigableMap<DateTime, List<Event>> cutMap = events.headMap(end, true).tailMap(start, true);
		for (Map.Entry<DateTime, List<Event>> entry : cutMap.entrySet()) {
			DateTime dt = entry.getKey();
			result.put(dt, entry.getValue());
		}

		return result;
	}
}
