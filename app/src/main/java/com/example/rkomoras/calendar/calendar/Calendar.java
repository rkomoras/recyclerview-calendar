package com.example.rkomoras.calendar.calendar;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.example.rkomoras.calendar.model.Event;
import com.example.rkomoras.calendar.util.Pair;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;
import java.util.Map;

public class Calendar extends LinearLayout {
	public static final int MAX_MONTH_HEIGHT = 6;
	private CalendarRecyclerView recyclerView;
	private static DateTimeFormatter monthFormatter = DateTimeFormat.forPattern("MMM y");

	private Map<DateTime, List<Event>> defaultData;

	public Calendar(Context context) {
		this(context, null);
	}

	public Calendar(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public Calendar(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public void init(CalendarAdapter.CalendarListener listener, @Nullable DateTime selectedDate, @Nullable YearMonth selectedMonth, @Nullable Map<DateTime, List<Event>> data) {
		recyclerView = new CalendarRecyclerView(getContext());
		addView(recyclerView);
		recyclerView.setAdapter(new CalendarAdapter(listener, selectedDate, selectedMonth, data));
		defaultData = null;
	}

	public DateTime getSelectedDate() {
		return recyclerView.getSelectedDate();
	}

	public YearMonth getSelectedMonth() {
		return recyclerView.getSelectedMonth();
	}

	public void addEvent(Event event) {
		recyclerView.addEvent(event);
	}

	public Pair<DateTime, DateTime> getDateRange() {
		if (recyclerView == null) {
			return CalendarAdapter.getDefaultDateRange();
		}
		return recyclerView.getDateRange();
	}

	public void setEvents(Map<DateTime, List<Event>> data) {
		if (recyclerView != null) {
			recyclerView.setEvents(data);
		}
	}

	public void goToToday() {
		recyclerView.goToToday();
	}

	public static String getMonthString(YearMonth month) {
		return monthFormatter.print(month);
	}
}
