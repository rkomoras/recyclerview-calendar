package com.example.rkomoras.calendar.calendar;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.rkomoras.calendar.R;
import com.example.rkomoras.calendar.model.Event;
import com.example.rkomoras.calendar.util.Pair;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Days;
import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.DayViewHolder> {
	private static final int SINGLE_VIEW_SIZE = Calendar.MAX_MONTH_HEIGHT * DateTimeConstants.DAYS_PER_WEEK;
	private static final int DEFAULT_VIEW_SIZE = 5 * SINGLE_VIEW_SIZE;

	private int itemCount;
	private int firstNewPosition = 0;
	private int firstVisiblePosition = 0;
	private DateTime firstMonday;
	private Pair<Integer, DateTime> selectedDate;
	private YearMonth selectedMonth;
	public static DateTimeFormatter debugFormatter = DateTimeFormat.forPattern("dd. MM. yyyy");
	private ListenerView listener;
	private Map<DateTime, List<Event>> events;

	private CalendarListener calListener;

	private int hiddenBgColor = Color.parseColor("#ffffff");
	private int hiddenTextColor = Color.parseColor("#888888");
	private int selMonthBgColor = Color.parseColor("#dedede");
	private int selMonthTextColor = Color.parseColor("#000000");
	private int selDayBgColor = Color.parseColor("#7d3cbe");
	private int selDayTextColor = Color.parseColor("#ffffff");
	private int eventTitleColor = Color.parseColor("#ffffff");

	public CalendarAdapter(CalendarListener listener, @Nullable DateTime selectedDate, @Nullable YearMonth selectedMonth, @Nullable Map<DateTime, List<Event>> data) {
		// TODO: DateTime ma metodu withFields - tam se da YearMonth

		this.calListener = listener;
		if (data != null) {
			events = data;
		} else {
			events = new HashMap<>();
		}
		itemCount = DEFAULT_VIEW_SIZE;
		DateTime today = new DateTime().withTimeAtStartOfDay();
		if (selectedDate == null) {
			DateTime todayM2M = today.minusMonths(2);
			this.selectedMonth = new YearMonth();
			firstMonday = new DateTime(todayM2M.getYear(), todayM2M.getMonthOfYear(), 1, 0 , 0).withDayOfWeek(1);
		} else {
			this.selectedMonth = new YearMonth(selectedDate.getYear(), selectedDate.getMonthOfYear());
			firstMonday = new DateTime(selectedDate.minusMonths(2).getYear(), selectedDate.minusMonths(2).getMonthOfYear(), 1, 0 , 0).withDayOfWeek(1);
		}

		if (selectedMonth != null) {
			this.selectedMonth = selectedMonth;

			// kontrola, zda je v rozsahu
			Pair<DateTime, DateTime> range = getDateRange();
			if (new DateTime(this.selectedMonth.getYear(), this.selectedMonth.getMonthOfYear(), 1, 0, 0, 0).isBefore(range.first)) {
				// je pred rozsahem
				DateTime newFirstMonday = new DateTime(this.selectedMonth.getYear(), this.selectedMonth.getMonthOfYear(), 1, 0, 0, 0).withDayOfWeek(1);
				itemCount += Days.daysBetween(newFirstMonday, firstMonday).getDays();
				firstMonday = newFirstMonday;
			} else if (new DateTime(this.selectedMonth.getYear(), this.selectedMonth.getMonthOfYear(), 1, 0, 0, 0).plusMonths(1).minusDays(1).isAfter(range.second)) {
				// je za rozsahem
				itemCount = Days.daysBetween(firstMonday, new DateTime(this.selectedMonth.getYear(), this.selectedMonth.getMonthOfYear(), 1, 0, 0, 0).plusMonths(1).minusDays(1).withDayOfWeek(DateTimeConstants.SUNDAY)).getDays();
			}
		}

		listener.notifyMonthChanged(this.selectedMonth);

		int selDatePosition = Days.daysBetween(firstMonday, (selectedDate == null) ? today : selectedDate).getDays();
		this.selectedDate = new Pair<>(selDatePosition, today);
		firstVisiblePosition = getDatePosition(new DateTime(today.getYear(), today.getMonthOfYear(), 1, 0, 0, 0).withDayOfWeek(1));

		setHasStableIds(true);

//		Log.v("FIRST MONDAY", debugFormatter.print(firstMonday));
//		Log.v("ITEM COUNT", Integer.toString(itemCount));
//		Log.v("SELECTED DATE", "Position: " + this.selectedDate.first + ", date: " + debugFormatter.print(this.selectedDate.second));
	}

	public static Pair<DateTime, DateTime> getDefaultDateRange() {
		DateTime todayM2M = new DateTime().minusMonths(2).withTime(0, 0, 0, 0);
		DateTime fm = new DateTime(todayM2M.getYear(), todayM2M.getMonthOfYear(), 1, 0 , 0).withDayOfWeek(1);
		return new Pair<>(fm, fm.plusDays(DEFAULT_VIEW_SIZE));
	}

	public Pair<DateTime, DateTime> getDateRange() {
		return new Pair<>(firstMonday, firstMonday.plusDays(itemCount));
	}

	public void setListenerView(ListenerView ocl) {
		this.listener = ocl;
		int position = Days.daysBetween(firstMonday, new DateTime(selectedMonth.getYear(), selectedMonth.getMonthOfYear(), 1, 0, 0, 0, 0)).getDays();
		CalendarAdapter.this.listener.scrollToItem(position);
		firstVisiblePosition = position;
	}

	@Override
	public int getItemCount() {
		return itemCount;
	}

	@Override
	public void onBindViewHolder(final DayViewHolder holder, final int position) {
		if (position == selectedDate.first) {
			holder.itemView.setBackgroundColor(selDayBgColor);
			holder.text.setTextColor(selDayTextColor);
			Animation a = AnimationUtils.loadAnimation(holder.itemView.getContext().getApplicationContext(), R.anim.blink);
			holder.itemView.startAnimation(a);
		} else if (getMonthPosition(position) == 0) {
			holder.itemView.setBackgroundColor(selMonthBgColor);
			holder.text.setTextColor(selMonthTextColor);
		} else {
			holder.itemView.setBackgroundColor(hiddenBgColor);
			holder.text.setTextColor(hiddenTextColor);
		}

		DateTime date = getDateFromPosition(position);
		holder.events.removeAllViews();
		if (events.get(date) != null && events.get(date).size() > 0) {
			for (Event event : events.get(date)) {
				TextView tv = new TextView(holder.itemView.getContext().getApplicationContext());
				tv.setText(event.getTitle());
				tv.setBackgroundColor(event.getColor());
				tv.setTextColor(eventTitleColor);
				tv.setTextSize(9.0f);
				tv.setMaxLines(1);
				tv.setEllipsize(TextUtils.TruncateAt.END);
				holder.events.addView(tv);
			}
		}

		final DateTime day = firstMonday.plusDays(position);
		holder.text.setText(Integer.toString(day.getDayOfMonth()));
		holder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				int move = selectItem(position);
				if (move != 0 && listener != null) {
					listener.move(move > 0);
				}
				holder.itemView.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
				holder.itemView.playSoundEffect(SoundEffectConstants.CLICK);
			}
		});
	}

	@Override
	public DayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new DayViewHolder(parent);
	}

	public int move(boolean forward) {
		int size = getScrollSize(forward);

		if (forward) {
			selectedMonth = selectedMonth.plusMonths(1);
		} else {
			selectedMonth = selectedMonth.minusMonths(1);
		}
		notifyItemRangeChanged(firstNewPosition + (forward ? 0 : -Calendar.MAX_MONTH_HEIGHT * DateTimeConstants.DAYS_PER_WEEK), 2 * Calendar.MAX_MONTH_HEIGHT * DateTimeConstants.DAYS_PER_WEEK);

		firstVisiblePosition += (forward ? (size * DateTimeConstants.DAYS_PER_WEEK) : (-size * DateTimeConstants.DAYS_PER_WEEK));

		if (firstVisiblePosition < Calendar.MAX_MONTH_HEIGHT * DateTimeConstants.DAYS_PER_WEEK) {
			// pridat data smerem zpet
			addDays(false);
		}

		if (firstVisiblePosition > itemCount - Calendar.MAX_MONTH_HEIGHT * DateTimeConstants.DAYS_PER_WEEK) {
			// pridat data smerem dopredu
			addDays(true);
		}

		Log.v("MOVE", "Current month: " + debugFormatter.print(selectedMonth) + ", first visible position: " + Integer.toString(firstVisiblePosition));
		Log.v("DATE RANGE", debugFormatter.print(getDateRange().first) + " - " + debugFormatter.print(getDateRange().second));

		calListener.notifyMonthChanged(selectedMonth);

		return size;
	}

	private void addDays(boolean forward) {
//		Log.v("ADDING DAYS", Boolean.toString(forward));

		if (forward) {
			int prevItemCount = itemCount;
			itemCount += 2 * Calendar.MAX_MONTH_HEIGHT * DateTimeConstants.DAYS_PER_WEEK;
			calListener.notifyAdd(firstMonday.plusDays(prevItemCount + 1), firstMonday.plusDays(itemCount));
		} else {
			notifyItemRangeInserted(0, 2 * Calendar.MAX_MONTH_HEIGHT * DateTimeConstants.DAYS_PER_WEEK);
			itemCount += 2 * Calendar.MAX_MONTH_HEIGHT * DateTimeConstants.DAYS_PER_WEEK;
			firstMonday = firstMonday.minusWeeks(2 * Calendar.MAX_MONTH_HEIGHT);
			firstVisiblePosition += 2 * Calendar.MAX_MONTH_HEIGHT * DateTimeConstants.DAYS_PER_WEEK;
			selectedDate.first += 2 * Calendar.MAX_MONTH_HEIGHT * DateTimeConstants.DAYS_PER_WEEK;
			calListener.notifyAdd(firstMonday, firstMonday.plusDays(2 * Calendar.MAX_MONTH_HEIGHT * DateTimeConstants.DAYS_PER_WEEK - 1));
		}
	}

	/**
	 * Method for getting position of date in the recycler view.
	 * @param date
	 * @return position
	 */
	public int getDatePosition(DateTime date) {
		int diff = Days.daysBetween(firstMonday.toLocalDate(), date.toLocalDate()).getDays();
		return date.isBefore(firstMonday) ? -diff : diff;
	}

	public DateTime getDateFromPosition(int position) {
		return firstMonday.plusDays(position);
	}

	/**
	 * Porovna prvek na pozici position a vrati vysledek podle mesice
	 * (zde resim jenom sousedni mesice)
	 * @param position
	 * @return 0 - pokud je ve stejnem mesici / 1 - pokud je po nem / -1 - pokud je pred nim
	 */
	public int getMonthPosition(int position) {
		DateTime day = firstMonday.plusDays(position);
		int selMonth = selectedMonth.getMonthOfYear();
		int newMonth = day.getMonthOfYear();
		if (newMonth == selMonth) {
			return 0;
		}

		return (newMonth == selMonth - 1 || (selMonth == DateTimeConstants.JANUARY && newMonth == DateTimeConstants.DECEMBER)) ? -1 : 1;
	}

	public void setFirstNewPosition(int position) {
		firstNewPosition = position;
	}

	/**
	 * Pocet tydnu, o ktery se musi odscrollovat
	 * @param forward dopredu nebo dozadu
	 * @return pocet tydnu
	 */
	private int getScrollSize(boolean forward) {
		DateTime curdt = new DateTime(selectedMonth.getValue(0), selectedMonth.getValue(1), 1, 0, 0);
		YearMonth newMonth = forward ? selectedMonth.plusMonths(1) : selectedMonth.minusMonths(1);
		DateTime tdt = new DateTime(newMonth.getValue(0), newMonth.getValue(1), 1, 0, 0);
		int daysInNewMonth = tdt.dayOfMonth().getMaximumValue();
		int firstDayNewMonth = tdt.getDayOfWeek();
		int daysThisMonth = curdt.dayOfMonth().getMaximumValue();
		int firstDayThisMonth = curdt.getDayOfWeek();

		int result;
		if (forward) {
			result = (int) Math.ceil((double) (daysThisMonth + firstDayThisMonth - 1) / DateTimeConstants.DAYS_PER_WEEK);
			if (firstDayNewMonth != 1) {
				result--;
			}
		} else {
			result = (int) Math.ceil((double) (daysInNewMonth + firstDayNewMonth - 1) / DateTimeConstants.DAYS_PER_WEEK);
			if (firstDayThisMonth != 1) {
				result--;
			}
		}
		return result;
	}

	public static class DayViewHolder extends RecyclerView.ViewHolder {
		TextView text;
		LinearLayout events;

		public DayViewHolder(View view) {
			super(LayoutInflater.from(view.getContext()).inflate(R.layout.day, (ViewGroup) view, false));
			itemView.setMinimumHeight((view.getMeasuredHeight() - (Calendar.MAX_MONTH_HEIGHT - 1) * view.getContext().getResources().getDimensionPixelSize(R.dimen.grid_spacing))  / Calendar.MAX_MONTH_HEIGHT);
			text = (TextView) itemView.findViewById(R.id.text);
			events = (LinearLayout) itemView.findViewById(R.id.briefEventList);
		}
	}

	@Override
	public long getItemId(int position) {
		return Integer.valueOf(position).hashCode();
	}

	private int selectItem(int position) {
		notifyItemChanged(selectedDate.first);
		selectedDate.first = position;
		selectedDate.second = firstMonday.plusDays(position);
		notifyItemChanged(selectedDate.first);

		return getMonthPosition(position);
		/*
		if (monthPosition == 0) {
			notifyItemChanged(selectedDate.first);
		} else if (!recyclerView.scrolling) {
			recyclerView.move(monthPosition > 0);
		}
		*/
	}

	public DateTime getSelectedDate() {
		return selectedDate.second;
	}

	public YearMonth getSelectedMonth() {
		return selectedMonth;
	}

	public interface ListenerView {
		void move(boolean forward);
		void scrollToItem(int position);
	}

	public void addEvent(Event event) {
		DateTime startTime = event.getStartTime();
		DateTime date = new DateTime(startTime.getYear(), startTime.getMonthOfYear(), startTime.getDayOfMonth(), 0, 0, 0);
		List<Event> le = events.get(date);
		if (le == null) {
			le = new ArrayList<>();
		}
		le.add(event);
		events.put(date, le);
		notifyEventAdded(date);
	}

	public void setEvents(Map<DateTime, List<Event>> events) {
		this.events.putAll(events);
	}

	private void notifyEventAdded(DateTime date) {
		int position = getDatePosition(date);
//		Log.v("EVENT ADDED", "At position " + Integer.toString(position));
		if (position >= 0 && position < itemCount) {
			notifyItemChanged(position);
		}
	}

	public interface CalendarListener {
		void notifyAdd(DateTime start, DateTime end);
		void notifyMonthChanged(YearMonth month);
	}

	/**
	 * Nastavi dnesek jako vybrany den
	 * @return pozice, na kterou se ma odscrollovat, nebo -1, pokud se scrollovat nema.
	 */
	public int goToToday() {
		DateTime today = new DateTime().withTimeAtStartOfDay();
		int position = getDatePosition(today);
//		Log.v("TODAY POSITION", Integer.toString(position));

		notifyItemChanged(selectedDate.first);
		selectedDate.first = position;
		selectedDate.second = firstMonday.plusDays(position);
		notifyItemChanged(selectedDate.first);

		if (getMonthPosition(position) == 0) {
			return -1;
		} else {
			selectedMonth = new YearMonth(today.getYear(), today.getMonthOfYear());
			DateTime firstVisibleDay = today.withDayOfMonth(1).withDayOfWeek(1);
			int ndPos = getDatePosition(firstVisibleDay);
			notifyItemRangeChanged(ndPos, SINGLE_VIEW_SIZE);
			calListener.notifyMonthChanged(selectedMonth);
			return ndPos;
		}
	}
}
